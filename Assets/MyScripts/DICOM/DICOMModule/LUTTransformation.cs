﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate T2 PixelFunction<T1, T2>(T1 pixel);

public class LUTTrasformation<T1, T2> //: IImageTransformation<T2>
{
    /*private AImage<T1> input;
    private AImage<T2> lut;
    private AImage<T2> output;

    //devo implementare anche il costruttore che prende la lut già pronta, estratta dal DICOM

    public LUTTrasformation(AImage<T1> input, PixelFunction<T2, T2> pf)
    {
        this.input = input;//deep copy della input? no, tanto non la modifico
        lut = new AImage<T2>(T1.GetMaxValueT1()/2, 2);
        output = new AImage<T2>(input.Width, input.Height);
        for (T2 i = 0; i < T2.GetMaxValue(); i++) //I can reduce the range to the min / max value of the image
        {
            T2 ris = pf(i);
            lut[i] = ris > T2.GetMaxValue() ? (T2)T2.GetMaxValue() : ris;   //should check for underflow too (under 0)
        }
    }

    public AImage<T2> Apply()
    {
        for (int i = 0; i < output.Width * output.Height; i++)
        {
            output[i] = lut[input[i]];
        }
        return output;
    }

    private T1 GetMaxValue()
    {
        object maxValue = default(T);
        TypeCode typeCode = Type.GetTypeCode(typeof(T));
        switch (typeCode)
        {
            case TypeCode.Byte:
                maxValue = byte.MaxValue;
                break;
            case TypeCode.UInt16:
                maxValue = ushort.MaxValue;
                break;
            default:
                maxValue = default(T);//set default value
                break;
        }

        return (T)maxValue;
    }

    private T2 GetMaxValue()
    {
        object maxValue = default(T);
        TypeCode typeCode = Type.GetTypeCode(typeof(T));
        switch (typeCode)
        {
            case TypeCode.Byte:
                maxValue = byte.MaxValue;
                break;
            case TypeCode.UInt16:
                maxValue = ushort.MaxValue;
                break;
            default:
                maxValue = default(T);//set default value
                break;
        }

        return (T)maxValue;
    }*/
}
