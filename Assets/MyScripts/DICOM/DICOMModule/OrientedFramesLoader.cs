﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is a loader that, given the DICOM directory path containing the DICOM datasets, outputs
/// the acquisition slices, with the spatial relations nedded to position them in the "patient coordinates
/// system".
/// </summary>
public class OrientedFramesLoader : AcquisitionFramesLoader
{
    protected SpatialInfoExtractor sie;

    public OrientedFramesLoader(string directoryPath) : base(directoryPath)
    {
    }

    protected override DICOMFrame LoadFrame(string filename)
    {
        this.sie = new SpatialInfoExtractor(new DICOMDataset(filename));
        return new DICOMFrame(pp.GetFrameAsTexture(), sie.GetTransformation());
    }
}
