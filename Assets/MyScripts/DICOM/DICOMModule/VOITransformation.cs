﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VOITransformation : IImageTransformation<byte> 
{
    private GreyscaleParameters gp;
    private AImage<ushort> input;

    public VOITransformation(GreyscaleParameters gp, AImage<ushort> input)
    {  
        this.gp = gp;
        this.input = input;
    }

    public byte LinearFunction(ushort inputPixel)
    {
        byte outPix;
        //linear VOI algorithm
        double half = ((gp.Window - 1) / 2.0) - 0.5;
        if (inputPixel <= gp.Level - half)
        {
            outPix = 0;
        }
        else if (inputPixel >= gp.Level + half)
        {
            outPix = 255;
        }
        else
        {
            outPix = (byte)(((inputPixel - (gp.Level - 0.5)) / (gp.Window - 1) + 0.5) * 255);
        }
        return outPix;
    }

    //should add sigmoidFunction

    public AImage<byte> Apply()
    {
        return new LUTByteTransformation(input, LinearFunction).Apply();
    }
}
