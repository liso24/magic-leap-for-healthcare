﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DICOMFrame
{
    public Sprite Sprite
    {
        get;
    }

    public Transformation3D Transformation
    {
        get;
    }

    public Texture2D AsTexture {
        get;
    }

    public DICOMFrame(Texture2D textureFrame, Transformation3D transformation)
    {
        AsTexture = textureFrame;
        Sprite = Sprite.Create(textureFrame, new Rect(0, 0, textureFrame.width, textureFrame.height), new Vector2(0.0f, 0.0f)); //third par is pivot point
        Transformation = transformation;
    }
}
