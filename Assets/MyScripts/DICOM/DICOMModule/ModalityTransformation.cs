﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModalityTransformation : IImageTransformation<ushort>
{
    //magari introdurre proprietà (set dei parametri) così si evita di istanziare sempre nuovi oggetti
    private GreyscaleParameters gp;
    private AImage<ushort> input;

    public ModalityTransformation(GreyscaleParameters gp, AImage<ushort> input) {  //se ho tempo servirebbe fare una classe più fine, così questa classe prende i parametri anchedella modality lut
        this.gp = gp;
        this.input = input;
    }

    public AImage<ushort> Apply()
    {
        return new LUTIntTrasformation(input, valgray => (ushort)(gp.Slope * valgray + gp.Intercept)).Apply();
    }
}
