﻿using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class MyControl : MonoBehaviour
{
    [SerializeField]
    public PersistentBehavior persistentBehavior;
    public MLInput.Controller _controller;

    private GameObject _cube, _camera;
    private const float _distance = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        _cube = GameObject.Find("PersistentCube");
        _camera = Camera.main.gameObject;

        MLResult result = MLInput.Start();
        _controller = MLInput.GetController(MLInput.Hand.Left);
    }

    void OnDestroy()
    {
        if (MLInput.IsStarted)
        {
            MLInput.Stop();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_controller.TriggerValue > 0.2f)
        {
            _cube.transform.position = _camera.transform.position + _camera.transform.forward * _distance;
            _cube.transform.rotation = _camera.transform.rotation;
            persistentBehavior.UpdateBinding();
        }
    }
}