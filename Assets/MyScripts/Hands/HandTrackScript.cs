﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class HandTrackScript : MonoBehaviour
{
    public enum HandPoses { Ok, Finger, Thumb, OpenHand, Fist, NoPose };
    public HandPoses pose = HandPoses.NoPose;
    public Vector3[] pos;
    public GameObject sphereThumb, sphereIndex, sphereWrist;
    public GameObject objectToPlace;
    public Transform ctransform; // Camera's transform
    private MLHandTracking.HandKeyPose[] _gestures;
    // ref to last obj created
    private GameObject lastPlacedObject;
    // place obj
    private bool canIplace = false;
    // rotation
    private bool doRotate = false;
    private float speed = 1;
    // custom instantiate
    private CustomInstantiate ci;

    private void Start()
    {
        // custom instantiate
        ci = GameObject.Find("AppManager").GetComponent<CustomInstantiate>();
        // ML input
        MLHandTracking.Start();
        _gestures = new MLHandTracking.HandKeyPose[5];
        _gestures[0] = MLHandTracking.HandKeyPose.Ok;
        _gestures[1] = MLHandTracking.HandKeyPose.Finger;
        _gestures[2] = MLHandTracking.HandKeyPose.OpenHand;
        _gestures[3] = MLHandTracking.HandKeyPose.Fist;
        _gestures[4] = MLHandTracking.HandKeyPose.Thumb;
        MLHandTracking.KeyPoseManager.EnableKeyPoses(_gestures, true, false);
        pos = new Vector3[3];
    }
    private void OnDestroy()
    {
        MLHandTracking.Stop();
    }


    private void Update()
    {
        if (GetGesture(MLHandTracking.Left, MLHandTracking.HandKeyPose.Ok))
        {
            pose = HandPoses.Ok;
            if (canIplace)
            {
                GameObject placedObject;
                if (ci)
                {
                    placedObject = CustomInstantiate.customInstantiate(objectToPlace, pos[0], Quaternion.identity);
                } else
                {
                    placedObject = Instantiate(objectToPlace, pos[0], Quaternion.identity);
                }
                MeshRenderer mesh = placedObject.GetComponent<MeshRenderer>();
                //placedObject.transform.Translate(0, mesh.bounds.size.y, 0);
                placedObject.transform.rotation = Quaternion.LookRotation(-ctransform.forward, ctransform.up);
                lastPlacedObject = placedObject;    
                canIplace = false;
            }
        }
        else if (GetGesture(MLHandTracking.Left, MLHandTracking.HandKeyPose.Finger))
        {
            pose = HandPoses.Finger;
            Debug.Log("Finger");
        }
        else if (GetGesture(MLHandTracking.Left, MLHandTracking.HandKeyPose.OpenHand))
        {
            pose = HandPoses.OpenHand;
            doRotate = true;
        }
        else if (GetGesture(MLHandTracking.Left, MLHandTracking.HandKeyPose.Fist))
        {
            pose = HandPoses.Fist;
            doRotate = false;
        }
        else if (GetGesture(MLHandTracking.Left, MLHandTracking.HandKeyPose.Thumb))
        {
            pose = HandPoses.Thumb;
            canIplace = true;
        }
        else
        {
            pose = HandPoses.NoPose;
        }

        if (pose != HandPoses.NoPose) ShowPoints();

        if ( doRotate && lastPlacedObject != null )
        {
            lastPlacedObject.transform.Rotate(new Vector3(15, 30, 45) * speed * Time.deltaTime);
        }
    }

    private void ShowPoints()
    {
        // Left Hand Thumb tip
        pos[0] = MLHandTracking.Left.Thumb.KeyPoints[2].Position;
        // Left Hand Index finger tip 
        pos[1] = MLHandTracking.Left.Index.KeyPoints[2].Position;
        // Left Hand Wrist 
        pos[2] = MLHandTracking.Left.Wrist.KeyPoints[0].Position;
        sphereThumb.transform.position = pos[0];
        sphereIndex.transform.position = pos[1];
        sphereWrist.transform.position = pos[2];
    }

    private bool GetGesture(MLHandTracking.Hand hand, MLHandTracking.HandKeyPose type)
    {
        if (hand != null)
        {
            if (hand.KeyPose == type)
            {
                if (hand.HandKeyPoseConfidence > 0.9f)
                {
                    return true;
                }
            }
        }
        return false;
    }

}
