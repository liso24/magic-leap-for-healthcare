﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class InputComplete : MonoBehaviour
{
    private MLInput.Controller controller;
    public Vector3 attachPoint;
    private bool trigger;
    private GameObject selectedGameObject;

    private float _distance = 2.0f;

    public const int STEP = 30;
    private float speed = 1;
    private Vector3 SCALE_CHANGE = new Vector3(0.01f, 0.01f, 0.01f);

    // Start is called before the first frame update
    void Start()
    {
        MLInput.Start();
        controller = MLInput.GetController(MLInput.Hand.Left);
    }

    void UpdateTriggerInfo()
    {
        if ( controller.TriggerValue > 0.8f )
        {
            if ( trigger == true )
            {
                RaycastHit hit;
                if ( Physics.Raycast(controller.Position, transform.forward, out hit) )
                {
                    if ( hit.transform.gameObject.tag == "Interactable")
                    {
                        selectedGameObject = hit.transform.gameObject;
                        Rigidbody r = selectedGameObject.GetComponent<Rigidbody>();
                        if (r)
                        {
                            r.useGravity = false;
                        }
                        _distance = Vector3.Distance(controller.Position, hit.transform.position);
                        attachPoint = hit.transform.position;
                    }
                }
                trigger = false;
            }
        }
        if (controller.TriggerValue < 0.2f)
        {
            trigger = true;
            if (selectedGameObject != null )
            {
                // selectedGameObject.GetComponent<Rigidbody>().useGravity = true;
                Rigidbody r = selectedGameObject.GetComponent<Rigidbody>();
                if (r)
                {
                    r.useGravity = true;
                }
                selectedGameObject = null;
            }
        }
    }

    void UpdateGestures()
    {
        string gestureType = controller.CurrentTouchpadGesture.Type.ToString();
        string gestureState = controller.TouchpadGestureState.ToString();
        string gestureDirection = controller.CurrentTouchpadGesture.Direction.ToString();
        // print("type " + gestureType);
        // print("state " + gestureState);
        // print("direction " + gestureDirection);
        if (gestureType.ToLower() == "radialscroll" && gestureState.ToLower() != "end")
        {
            if (CustomInstantiate.parent != null)
            {
                int step = STEP;
                if (gestureDirection.ToLower() != "clockwise")
                {
                    step = -step;
                }
                CustomInstantiate.parent.transform.Rotate(new Vector3(0, step, 0) * speed * Time.deltaTime);
            }
        } else if (gestureType.ToLower() == "swipe" && gestureState.ToLower() != "end")
        {
            if (CustomInstantiate.parent != null)
            {
                Vector3 step = SCALE_CHANGE;
                if (gestureDirection.ToLower() == "down" || gestureDirection.ToLower() == "left")
                {
                    step = -step;
                }
                CustomInstantiate.parent.transform.transform.localScale += step;
            }
        }
    }

    void OnDestroy()
    {
        //Stop receiving input by the Control
        MLInput.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (controller != null)
        {
            transform.position = controller.Position;
            transform.rotation = controller.Orientation;

            if (selectedGameObject)
            {
                // selectedGameObject.transform.position = attachPoint;
                selectedGameObject.transform.position = transform.position + transform.forward * _distance;
                selectedGameObject.transform.rotation = transform.rotation;
                // slab excep
                if (selectedGameObject.name == "xray_lightbox")
                {
                    // selectedGameObject.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0,180,0));
                    selectedGameObject.transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y + 180, 0);
                }
            }
            UpdateTriggerInfo();
            UpdateGestures();
        }
    }
}
