﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;
using UnityEngine.UI;

public class ObjPlacer : MonoBehaviour
{
    public GameObject modelToPlace;
    public GameObject framesContainerPrefab;
    public GameObject pointOfInterestToPlace;

    // Dicom
    public GameObject slider;
    private GameObject framesContainerInstance;

    //check
    protected bool created = false;
    private GameObject lastPlacedObj;

    // Start is called before the first frame update
    void Start()
    {
        slider = GameObject.Find("Slider");
    }

    protected void placeModel(Vector3 position, Vector3 rotation)
    {
        // instantiate model 
        GameObject modelInstance = CustomInstantiate.customInstantiate(modelToPlace, position, Quaternion.Euler(rotation));
        MeshRenderer meshModel = modelInstance.GetComponent<MeshRenderer>();
        if (meshModel != null)
        {
            modelInstance.transform.Translate(0, meshModel.bounds.size.y, 0);
        }
        created = true;
        CustomInstantiate.parent = modelInstance;
        if (modelToPlace.tag == "DICOM")
        {
            print("Dicom Model Detected!");
            framesContainerInstance = Instantiate(framesContainerPrefab, position, Quaternion.Euler(rotation));

            // Make slider visible
            // slider.SetActive(true);

            //set the fiels of the ClippingModelFramesController instance
            framesContainerInstance.GetComponent<ClippingModelFramesController>().indexSlider = slider.GetComponent<Slider>();
            framesContainerInstance.GetComponent<ClippingModelFramesController>().segmentedModel = modelInstance;
            // translate
            MeshRenderer meshFrame = framesContainerInstance.GetComponent<MeshRenderer>();
            if (meshFrame != null)
            {
                framesContainerInstance.transform.Translate(0, meshModel.bounds.size.y, 0);
            }

            // modelInstance.transform.Rotate(0, 180, 0, Space.Self);// Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
        }
    }

    protected void placePointOfInterest(Vector3 position, Vector3 rotation)
    {  
        // custominstantiate will assign parent
        GameObject pointObj = CustomInstantiate.customInstantiate(pointOfInterestToPlace, position, Quaternion.Euler(rotation));
        lastPlacedObj = pointObj; 
    }

    // Update is called once per frame
    void Update()
    {
        if (lastPlacedObj != null)
        {
            print("World coordinates:" + lastPlacedObj.transform.position);
            // inversione coordinate
            Transform t = CustomInstantiate.parent.transform;
            Vector3 ObjRelative = t.InverseTransformPoint(lastPlacedObj.transform.position);
            print("Realtive coordinates:" + ObjRelative);
        }
    }
}
