﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class InputCompleteController : ObjPlacer
{
    private MLInput.Controller controller;
    public Vector3 attachPoint;
    public bool singleCreate;
    private bool trigger;
    private GameObject selectedGameObject;
    private float _distance = 2.0f;
    public const int STEP = 30;
    private float speed = 1;
    private Vector3 SCALE_CHANGE = new Vector3(0.01f, 0.01f, 0.01f);

    // Start is called before the first frame update
    void Start()
    {
        MLInput.Start();
        MLInput.OnControllerButtonDown += OnButtonDown;
        controller = MLInput.GetController(MLInput.Hand.Left);
    }

    void OnButtonDown(byte controller_id, MLInput.Controller.Button button)
    {
        if (button == MLInput.Controller.Button.Bumper)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                if (!(singleCreate && created))
                {
                    placeModel(hit.point, hit.normal);
                }
                else
                {
                    // If hitted obj is the placed one
                    if (hit.transform.gameObject.name == CustomInstantiate.parent.name)
                    {
                        placePointOfInterest(hit.point, hit.normal);
                    }
                }
            }
        }
    }

    void OnDestroy()
    {
        //Stop receiving input by the Control
        MLInput.Stop();
        MLInput.OnControllerButtonDown -= OnButtonDown;
    }

    // Update is called once per frame
    void Update()
    {
        if (controller != null)
        {
            transform.position = controller.Position;
            transform.rotation = controller.Orientation;

            if (selectedGameObject)
            {
                // selectedGameObject.transform.position = attachPoint;
                selectedGameObject.transform.position = transform.position + transform.forward * _distance;
                selectedGameObject.transform.rotation = transform.rotation;
            }
            UpdateTriggerInfo();
            UpdateGestures();
        }
    }

    void UpdateGestures()
    {
        string gestureType = controller.CurrentTouchpadGesture.Type.ToString();
        string gestureState = controller.TouchpadGestureState.ToString();
        string gestureDirection = controller.CurrentTouchpadGesture.Direction.ToString();
        // print("type " + gestureType);
        // print("state " + gestureState);
        // print("direction " + gestureDirection);
        if (gestureType.ToLower() == "radialscroll" && gestureState.ToLower() != "end")
        {
            if (CustomInstantiate.parent != null)
            {
                int step = STEP;
                if (gestureDirection.ToLower() != "clockwise")
                {
                    step = -step;
                }
                CustomInstantiate.parent.transform.Rotate(new Vector3(0, step, 0) * speed * Time.deltaTime);
            }
        }
        else if (gestureType.ToLower() == "swipe" && gestureState.ToLower() != "end")
        {
            if (CustomInstantiate.parent != null)
            {
                Vector3 step = SCALE_CHANGE;
                if (gestureDirection.ToLower() == "down" || gestureDirection.ToLower() == "left")
                {
                    step = -step;
                }
                CustomInstantiate.parent.transform.transform.localScale += step;
            }
        }
    }

    void UpdateTriggerInfo()
    {
        if (controller.TriggerValue > 0.8f)
        {
            if (trigger == true)
            {
                RaycastHit hit;
                if (Physics.Raycast(controller.Position, transform.forward, out hit))
                {
                    if (hit.transform.gameObject.tag == "Interactable" || hit.transform.gameObject.tag == "DICOM")
                    {
                        selectedGameObject = hit.transform.gameObject;
                        Rigidbody r = selectedGameObject.GetComponent<Rigidbody>();
                        if (r)
                        {
                            r.useGravity = false;
                        }
                        _distance = Vector3.Distance(controller.Position, hit.transform.position);
                        attachPoint = hit.transform.position;
                    }
                    else
                    {
                        print("You can't interact with this object!");
                    }
                }
                trigger = false;
            }
        }
        if (controller.TriggerValue < 0.2f)
        {
            trigger = true;

            // release obj..
            if (selectedGameObject != null)
            {
                Rigidbody r = selectedGameObject.GetComponent<Rigidbody>();
                if (r)
                {
                    r.useGravity = true;
                }
                selectedGameObject = null;
            }
        }
    }

}
