﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;
using UnityEngine.UI;

public class PlacePoints : MonoBehaviour
{
    public GameObject objectToPlace;
    public GameObject pointToPlace;
    public bool singleCreate;
    // private GameObject pointText;
    private bool created;
    private MLInput.Controller controller;

    //check
    private GameObject lastPlacedObj;

    // Start is called before the first frame update
    void Start()
    {
        MLInput.Start();
        MLInput.OnControllerButtonDown += OnButtonDown;
        controller = MLInput.GetController(MLInput.Hand.Left);
        /*pointText = new GameObject();
        TextMesh textMesh = new TextMesh(); */
        //pointToPlace.AddComponent<TextMesh>();
    }

    void OnButtonDown(byte controller_id, MLInput.Controller.Button button)
    {
        if (button == MLInput.Controller.Button.Bumper)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                Vector3 point = hit.point;
                if (!(singleCreate && created) )
                {
                    /*Vector3 objectSize = objectToPlace.GetComponent<MeshRenderer>().bounds.size;
                    point += new Vector3(0, objectSize.y/2, 0);*/
                    GameObject placeObject = CustomInstantiate.customInstantiate(objectToPlace, hit.point, Quaternion.Euler(hit.normal));
                    MeshRenderer mesh = placeObject.GetComponent<MeshRenderer>();
                    if (mesh != null)
                    {
                        placeObject.transform.Translate(0, mesh.bounds.size.y, 0);
                    }
                    created = true;
                    CustomInstantiate.parent = placeObject;
                    // mesh.bounds.size.y += mesh.bounds.size.y;
                }
                else
                {
                    // If hitted obj is the placed one
                    if (hit.transform.gameObject.name == CustomInstantiate.parent.name)
                    {
                        // custominstantiate will assign parent
                        GameObject pointObj = CustomInstantiate.customInstantiate(pointToPlace, hit.point, Quaternion.Euler(hit.normal));
                        lastPlacedObj = pointObj;
                        print("Created coordinates:" + lastPlacedObj.transform.position);
                        //GameObject textObj = CustomInstantiate.customInstantiate(pointText, hit.point, Quaternion.Euler(hit.normal));
                        //Grabs the TextMesh component from the game object
                        // TextMesh text = pointObj.GetComponent<TextMesh>();
                        //Sets the text.
                        /* text.text = "" + hit.point;
                        
                        text.fontSize = 30;
                        text.color = Color.red; // Set the text's color to red */
                    }
                }
            }
        }
    }

    void OnDestroy()
    {
        // Stop receiving input by the Control
        MLInput.Stop();
        MLInput.OnControllerButtonDown -= OnButtonDown;
    }


    // Update is called once per frame
    void Update()
    {
        if (lastPlacedObj != null)
        {
            print("World coordinates:" + lastPlacedObj.transform.position);
            // inversione coordinate
            Transform t = CustomInstantiate.parent.transform;
            Vector3 ObjRelative = t.InverseTransformPoint(lastPlacedObj.transform.position);
            print("Realtive coordinates:" + ObjRelative);
        }
    }
}
