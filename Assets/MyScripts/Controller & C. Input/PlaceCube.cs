﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;

public class PlaceCube : MonoBehaviour
{
    public GameObject objectToPlace;
    public bool singleCreate;
    private bool created;
    private MLInput.Controller controller;
    // Start is called before the first frame update
    void Start()
    {
        MLInput.Start();
        MLInput.OnControllerButtonDown += OnButtonDown;
        controller = MLInput.GetController(MLInput.Hand.Left);
    }

    void OnButtonDown(byte controller_id, MLInput.Controller.Button button)
    {
        if (button == MLInput.Controller.Button.Bumper && !(singleCreate && created))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                Vector3 point = hit.point;
                /*Vector3 objectSize = objectToPlace.GetComponent<MeshRenderer>().bounds.size;
                point += new Vector3(0, objectSize.y/2, 0);*/
                GameObject placeObject = CustomInstantiate.customInstantiate(objectToPlace, hit.point, Quaternion.Euler(hit.normal));
                MeshRenderer mesh = placeObject.GetComponent<MeshRenderer>();
                if (mesh != null)
                {
                    placeObject.transform.Translate(0, mesh.bounds.size.y, 0);
                }
                created = true;
                //mesh.bounds.size.y += mesh.bounds.size.y;
            }
        }
    }

    void OnDestroy()
    {
        //Stop receiving input by the Control
        MLInput.Stop();
        MLInput.OnControllerButtonDown -= OnButtonDown;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
