﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEditor;
using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;

[Serializable]
public class MyCube
{
	public string name;
	public float speed;
	public string color;
}

[Serializable]
public class MyList
{
	[SerializeField]
	public List<MyCube> listCubes = new List<MyCube>();
	public Dictionary<string, MyCube> namesDict = new Dictionary<string, MyCube>();
}

public interface IVisitor
{
	void doAction(SpawnAction action);
	void doAction(UpdateAction action);
}

public interface IAction
{
	void AcceptAction(IVisitor visitor);
}

public class UpdateAction : IAction
{
	public string Name { get; set; }
	public float NewSpeed { get; set; }
	public string Color { get; set; }

	public void AcceptAction(IVisitor visitor)
	{
		visitor.doAction(this);
	}
}

public class SpawnAction : IAction
{
	public Vector3 Position { get; set; }
	public Quaternion Rotation { get; set; }
	public float Speed { get; set; }

	public void AcceptAction(IVisitor visitor)
	{
		visitor.doAction(this);
	}
}

public class ProvaAPI : MonoBehaviour, IVisitor
{
	public GameObject objectToPlace;
	public Transform parentObject;
	public int spawnCounter = 0;
	// Construct a ConcurrentQueue.
	ConcurrentQueue<IAction> cq = new ConcurrentQueue<IAction>();
	MyList cubeList = new MyList();

	private HttpListener listener;
	private Thread listenerThread;

	private Dictionary<string, Color> stringToColor = new Dictionary<string, Color>();
	private Dictionary<Color, string> colorToString = new Dictionary<Color, string>();

	void Start()
	{
		// fill stringToColor
		stringToColor.Add("Rosso", Color.red);
		stringToColor.Add("Giallo", Color.yellow);
		stringToColor.Add("Blu", Color.blue);
		// fill colorToString
		colorToString.Add(Color.red, "Rosso");
		colorToString.Add(Color.yellow, "Giallo");
		colorToString.Add(Color.blue, "Blu");
		// start server
		listener = new HttpListener();
		listener.Prefixes.Add("http://localhost:4444/");
		listener.Prefixes.Add("http://127.0.0.1:4444/");
		listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
		listener.Start();

		listenerThread = new Thread(startListener);
		listenerThread.Start();
		Debug.Log("Server Started");
	}

	void Update()
	{

		IAction action;
		while (cq.TryDequeue(out action))
		{
			Debug.Log("action dequeued");
			if (action is SpawnAction spawnAction)
			{
				doAction(spawnAction);
			} else if (action is UpdateAction updateAction)
			{
				doAction(updateAction);
			} else
            {
				Debug.Log("Type not found");
			}
		}

		updateListGameObjects();

	}

	private void updateListGameObjects()
    {
		// Debug.Log("parent: " + parentObject);
		foreach (Transform child in parentObject)
		{
			RotationCube script = child.gameObject.GetComponent<RotationCube>();
			if (child.gameObject.tag == "Interactable" && script != null)
			{
				var name = child.gameObject.name;
				// Debug.Log("object name: " + name);
				if ( !cubeList.namesDict.ContainsKey(name))
                {
					MyCube cube = new MyCube();
					cube.speed = script.speed;
					cube.name = name;
					Renderer renderer = child.gameObject.GetComponent<Renderer>();
					if (renderer != null)
                    {
						string colorName;
						if (colorToString.TryGetValue(renderer.material.color, out colorName))
						{
							cube.color = colorName;
						} else
                        {
							Debug.Log("Colore non trovato");
							renderer.material.color = Color.red;
							cube.color = "Rosso";
						}
					}
					// cubeList.listCubes.Add(name);
					cubeList.namesDict.Add(name, cube);
					cubeList.listCubes.Add(cube);
				} else
                {
					// update
					MyCube cube;
					if (cubeList.namesDict.TryGetValue(name, out cube))
                    {
						cube.speed = script.speed;
					}
					Renderer renderer = child.gameObject.GetComponent<Renderer>();
					if (renderer != null)
					{
						string colorName;
						if (colorToString.TryGetValue(renderer.material.color, out colorName))
						{
							cube.color = colorName;
						}
						else
						{
							Debug.Log("Colore non trovato");
							renderer.material.color = Color.red;
							cube.color = "Rosso";
						}
					}
				}
			}
		}
		// Debug.Log("list length: " + cubeList.Count);
	}

	private void startListener()
	{
		while (true)
		{
			var result = listener.BeginGetContext(ListenerCallback, listener);
			result.AsyncWaitHandle.WaitOne();
		}
	}

	private void ListenerCallback(IAsyncResult result)
	{
		var context = listener.EndGetContext(result);
		var method = context.Request.HttpMethod;
		var localUrl = context.Request.Url.LocalPath;

		Debug.Log("Method: " + method);
		Debug.Log("LocalUrl: " + localUrl);

		if (method == "GET")
        {	
			if ( localUrl == "/api/get/items/")
            {
				Debug.Log("items");
				Debug.Log("list length: " + cubeList.listCubes.Count);
				string jsonData = JsonUtility.ToJson(cubeList);
				Debug.Log("data: " + jsonData);
				// Obtain a response object.
				HttpListenerResponse response = context.Response;
				// Construct a response.
				byte[] buffer = System.Text.Encoding.UTF8.GetBytes(jsonData);
				// Get a response stream and write the response to it.
				response.ContentLength64 = buffer.Length;
				System.IO.Stream output = response.OutputStream;
				output.Write(buffer, 0, buffer.Length);
				output.Close();
			} else if (localUrl == "/api/set/")
			{
				string name = context.Request.QueryString["name"];
				string speed = context.Request.QueryString["speed"];
				string color = context.Request.QueryString["color"];
				float speedNumber;
				if (name != null && float.TryParse(speed, out speedNumber))
                {
					// create UpdateAction
					UpdateAction action = new UpdateAction();
					action.Name = name;
					action.Color = color;
					if (speedNumber > 0)
					{
						action.NewSpeed = speedNumber;
					}
					else
					{
						action.NewSpeed = 0.0f;
					}
					// Debug.Log("UpdateAction enqueued");
					cq.Enqueue(action);
				}
			} else if (localUrl == "/api/create/")
			{
				string speed = context.Request.QueryString["speed"];
				float speedNumber;
				if (float.TryParse(speed, out speedNumber))
				{
					// create SpawnAction
					SpawnAction action = new SpawnAction();
					action.Position = new Vector3(spawnCounter * (1.0f), 0, 1);
					action.Rotation = Quaternion.identity;
					if (speedNumber > 0)
					{
						action.Speed = speedNumber;
					} else
                    {
						action.Speed = 0.0f;
					}
					// Debug.Log("SpawnAction enqueued");
					cq.Enqueue(action);
					spawnCounter += 1;
				}
			}
		} else if (context.Request.HttpMethod == "POST")
		{
			Thread.Sleep(1000);
			var data_text = new StreamReader(context.Request.InputStream,
								context.Request.ContentEncoding).ReadToEnd();
			Debug.Log(data_text);
		}

		context.Response.Close();
	}

	// IVisitor stuff
	public void doAction(SpawnAction action)
	{
		Debug.Log("SpawnAction");
		GameObject placeObject = Instantiate(objectToPlace, action.Position, action.Rotation);
		if (parentObject != null)
		{
			placeObject.transform.SetParent(parentObject);
		}
		GameObjectUtility.EnsureUniqueNameForSibling(placeObject);
		if (action.Speed > 0)
		{
			placeObject.AddComponent<RotationCube>();
			placeObject.GetComponent<RotationCube>().speed = action.Speed;
		}
	}

	public void doAction(UpdateAction action)
	{
		Debug.Log("UpdateAction");
		GameObject objToUpdate = GameObject.Find(action.Name);
		RotationCube script = objToUpdate.GetComponent<RotationCube>();
		Renderer renderer = objToUpdate.GetComponent<Renderer>();
		if ( script != null )
        {
			script.speed = action.NewSpeed;
        }
		if ( renderer != null && !String.IsNullOrEmpty(action.Color))
        {
			Color color;
			if (stringToColor.TryGetValue(action.Color, out color))
            {
				renderer.material.color = color;
			}
			else
            {
				renderer.material.color = Color.red;
			}
		}
	}

}
