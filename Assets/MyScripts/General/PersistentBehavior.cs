﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.MagicLeap;
using MagicLeap.Core;

public class PersistentBehavior : MonoBehaviour
{
    [SerializeField]
    private string uniqueId = null;
    private TransformBinding binding = null;

    // Start is called before the first frame update
    void Start()
    {
        if (string.IsNullOrWhiteSpace(uniqueId))
        {
            Debug.LogError("Error: PersistentBehavior No uniqueId set.");
            enabled = false;
            return;
        }

    #if PLATFORM_LUMIN
        MLResult result = MLPersistentCoordinateFrames.Start();
        if (!result.IsOk)
        {
            Debug.LogErrorFormat("Error: PersistentBehavior failed starting MLPersistentCoordinateFrames, disabling script. Reason: {0}", result);
            enabled = false;
            return;
        }
        MLPersistentCoordinateFrames.OnLocalized += HandleOnLocalized;
    #endif
    }

    public void UpdateBinding()
    {
        if (binding == null || !binding.Update())
        {
            CreateContent();
        }
    }

    private void HandleOnLocalized(bool localized)
    {
    #if PLATFORM_LUMIN
        if (localized)
        {
            RegainAllStoredBindings();
        }
    #endif
    }

    private void RegainAllStoredBindings()
    {
    #if PLATFORM_LUMIN
        TransformBinding.storage.LoadFromFile();

        List<TransformBinding> allBindings = TransformBinding.storage.Bindings;
        foreach (TransformBinding storedBinding in allBindings)
        {
            // Try to find the PCF with the stored CFUID.
            MLResult result = MLPersistentCoordinateFrames.FindPCFByCFUID(storedBinding.PCF.CFUID, out MLPersistentCoordinateFrames.PCF pcf);
            if (storedBinding.Id == uniqueId && pcf != null && MLResult.IsOK(pcf.CurrentResultCode))
            {
                binding = storedBinding;
                binding.Bind(pcf, gameObject.transform, true);
            }
        }
    #endif
    }

    private void CreateContent()
    {
    #if PLATFORM_LUMIN
        MLPersistentCoordinateFrames.FindClosestPCF(gameObject.transform.position
            , out MLPersistentCoordinateFrames.PCF pcf);
        binding = new TransformBinding(uniqueId);
        binding.Bind(pcf, gameObject.transform);
    #endif
    }
}