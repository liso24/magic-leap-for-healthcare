﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class CustomInstantiate : MonoBehaviour
{
    public GameObject myParent;
    public static GameObject parent;
    public static GameObject customInstantiate(GameObject original, Vector3 position, Quaternion rotation)
    {
        GameObject placedObject;
        // If parent exists then the next obj must be his son
        if (parent && parent.transform)
        {
            placedObject = Instantiate(original, position, rotation, parent.transform);
        }
        else
        {
            placedObject = Instantiate(original, position, rotation);
        }
        GameObjectUtility.EnsureUniqueNameForSibling(placedObject);
        return placedObject;
    }

    // Start is called before the first frame update
    void Start()
    {
        CustomInstantiate.parent = myParent;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
