# Magic Leap for healthcare #

Progetto Unity con componenti MLTK e Magic Leap enviroment, utilizzato per:

* Visualizzazione file DICOM
* Visualizzazione e manipolazione modelli 3D
* Confronto tra DICOM file e modelli 3D

## Versioni utilizzate
* Unity 2020.2.2f1
* Visual Studio 2019

## Descrizione
All'interno della scena di Unity e del progetto sono presenti:

* Un modello 3D di Reni.
* Un DICOM visualizzato a sprite, non ancora allineato e messo a confronto con il modello 3D.
* Uno slider interattivo, ma attualmente non funzionante
* La libreria attualmente in utilizzo evilDICOM.

## Funzionamento e problematiche
Lanciando l'applicativo attraverso la preview di Unity e l'emulatore messo a disposizione da Magic Leap, Abbiamo un modello 3D di reni messo a confronto con il DICOM corrispondente.
Il modello e la sprite sono manipolabili attraverso l'apposito controller e anche attraverso la manipolazione con le mani.
utilizzando lo slider presente a schermo (non quello all'interno dell'ambiente in quanto non funzionante), vi è la possibilità di cambiare sprite DICOM a diversi livelli di profondità del modello 3D.
Il modello viene tagliato per poter effettuare più accuratamente il confronto con il DICOM stesso.
Problematiche ancora non risolte:

* Lo slider presente come oggetto nell'ambiente di lavoro ancora non funzionante
* Il deploy sul dispositivo Magic Leap non funzionante, possibili problemi con il deploy della libreria e della dll
